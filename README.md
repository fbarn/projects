# Beamsearch

Beamsearch: Standard beamsearch algorithm, impemented in R. User can configure the starting graph, 
beam width, and number of iterations. Exports a 3D plot of the graph as a PNG, and a CSV spreadsheat containing other data.

Depends on: [plot3D library](https://cran.r-project.org/web/packages/plot3D/index.html)

![screenshot](https://gitlab.com/fbarn/projects/raw/master/screenshots/beamSearch.png)
